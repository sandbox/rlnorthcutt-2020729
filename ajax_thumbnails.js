Drupal.behaviors.ajax_thumbnails = function (context) {
  $('#ajax-thumbnails-list a.ajax-thumbnails-switch').click(function() {
    // Start spinner
    $('#ajax-thumbnail-main').addClass('thumbnail-loader');

    // Get the imagecache settings
    var imagecacheMain = Drupal.settings.ajax_thumbnails.ajax_thumbnails_main;
    if (imagecacheMain.length > 1) { imagecacheMain = imagecacheMain[0];}
    var imagecacheThumb = Drupal.settings.ajax_thumbnails.ajax_thumbnails_thumbnail;
    if (imagecacheThumb.length > 1) { imagecacheThumb = imagecacheThumb[0];}

    // Get the image indexes
    var newIndex = $(this).attr('id').split('-');
    newIndex = newIndex[newIndex.length - 1];
    var oldIndex = $('#ajax-thumbnail-main img').attr('id').split('-');
    oldIndex = oldIndex[oldIndex.length - 1];

    // Get the path of the new full image
    var newImagePath = $('#ajax-thumbnails-list li#thumbnail-' + newIndex + ' img').attr('src').replace(imagecacheThumb, imagecacheMain);
    // Toggle the old and new thumbnails
    $('#ajax-thumbnails-list li').show();
    $('#ajax-thumbnails-list #thumbnail-' + newIndex).hide();
    // Replace the full image
    $('#ajax-thumbnail-main img').attr('src', newImagePath);

    // Turn off spinner
    var t = setTimeout(function(){
      $('#ajax-thumbnail-main').removeClass('thumbnail-loader')},500);

    return false;
  });
}
