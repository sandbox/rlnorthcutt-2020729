-- SUMMARY --

Image field formatter to provide thumbnail versions that update the main image.

When an image field has more than one image, this formatter will display the
first image with the others as thumbnails underneath it. When you click on the
thumbnail, it replaces the primary (and vice-versa). In the admin settings, you
can select which imagecache settings to use for the primary and the thumbnail
images.

-- REQUIREMENTS --

* Imagefield
* Imagecache

-- INSTALLATION --

* Enable module as usual.

-- CONFIGURATION --

* Go to admin/settings/ajax_thumbnails

-- TROUBLESHOOTING --


-- CONTACT --
Current maintainers:
* Ron Northcutt (rlnorthcutt) - http://drupal.org/user/23506
